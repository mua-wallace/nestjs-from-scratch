import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EntityClassOrSchema } from '@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        // type: 'postgres',
        // host: configService.getOrThrow('POSTGRES_HOST'),
        // port: configService.getOrThrow('POSTGRES_PORT'),
        // username: configService.getOrThrow('POSTGRES_USER'),
        // password: configService.getOrThrow('POSTGRES_PASSWORD'),
        // database: configService.getOrThrow('POSTGRES_DB'),
        // synchronize: configService.getOrThrow('POSTGRES_SYNCHRONIZE'),
        // autoLoadEntities: true,
        type: 'postgres',
        host: configService.get('POSTGRES_HOST'),
        port: configService.get('POSTGRES_PORT'),
        username: configService.get('POSTGRES_USER'),
        password: configService.get('POSTGRES_PASSWORD'),
        database: configService.get('POSTGRES_DB'),
        synchronize: configService.get('POSTGRES_SYNCHRONIZE'),
        autoLoadEntities: true,
      }),
    }),
  ],
})
export class DatabaseModule {
  static forFeature(model: EntityClassOrSchema[]) {
    return TypeOrmModule.forFeature(model);
  }
}
