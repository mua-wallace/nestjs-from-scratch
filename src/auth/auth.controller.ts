import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { UsersService } from 'src/users/users.service';
import { LocalAuthGuard } from './guards/local-auth.guards';
import { RefreshJwtAuthGuard } from './guards/refreshToken-auth.guard';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UsersService,
  ) {}

  @Post('/register')
  async register(@Body() registrationData: CreateUserDto) {
    return this.userService.createUser(registrationData);
  }
  @Get(':email')
  getPostById(@Param('email') email: string) {
    return this.userService.getByEmail(email);
  }

  @UseGuards(LocalAuthGuard)
  @Post('/login')
  async login(@Req() request) {
    return await this.authService.login(request.user);
  }

  @UseGuards(RefreshJwtAuthGuard)
  @Post('/refresh-token')
  async refreshToken(@Req() request) {
    return await this.authService.refreshToken(request.user);
  }
}
