import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { User } from './entities/create-user.entity';
import { EntityManager, Repository } from 'typeorm';
import { ICreateUser } from './interfaces/user.interface';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private entityManager: EntityManager,
  ) {}

  async getByEmail(email: string) {
    const user = await this.usersRepository.findOne({ where: { email } });
    if (!user) {
      throw new HttpException(
        'User with this email does not exist',
        HttpStatus.NOT_FOUND,
      );
    }
    return user;
  }

  async findOneWithEmail(email: string) {
    return await this.usersRepository.findOne({ where: { email } });
  }

  async getById(id: number) {
    const user = await this.usersRepository.findOne({ where: { id } });
    if (!user) {
      throw new HttpException('User does not exist', HttpStatus.NOT_FOUND);
    }
    return user;
  }

  async createUser(data: ICreateUser) {
    const newUser = this.usersRepository.create({
      ...data,
      password: await bcrypt.hash(data.password, 10),
    });
    await this.entityManager.save(newUser);
    return newUser;
  }

  getAllUsers() {
    return this.usersRepository.find({});
  }
}
