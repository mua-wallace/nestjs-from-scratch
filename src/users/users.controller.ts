import { Controller, Get, UseGuards } from '@nestjs/common';

import { UsersService } from './users.service';
import { JwtAthGuard } from 'src/auth/guards/jwt-auth.guard';

@Controller('user')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @UseGuards(JwtAthGuard)
  @Get('/all')
  getAllUsers() {
    return this.userService.getAllUsers();
  }
}
