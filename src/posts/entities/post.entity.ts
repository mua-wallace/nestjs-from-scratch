import { AbstractEntity } from 'src/database/abstract.entity';
import { Column, Entity } from 'typeorm';

@Entity()
class Post extends AbstractEntity<Post> {
  @Column()
  public title: string;

  @Column()
  public content: string;
}

export default Post;
